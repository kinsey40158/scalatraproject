FROM tomcat

MAINTAINER kinsey40

RUN apt-get update && apt-get clean

COPY target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/hello-scalatra.war

EXPOSE 8080
CMD ["catalina.sh", "run"]

